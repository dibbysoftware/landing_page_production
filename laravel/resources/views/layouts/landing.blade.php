<!DOCTYPE html>
<html>
<head>
	<!-- Basic -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">	

		<title>Corporate</title>	

		<meta name="keywords" content="HTML5 Template" />
		<meta name="description" content="Porto - Responsive HTML5 Template">
		<meta name="author" content="okler.net">

		<!-- Favicon -->
		<link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
		<link rel="apple-touch-icon" href="img/apple-touch-icon.png">

		<!-- Mobile Metas -->
		<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<!-- Web Fonts  -->
		<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,700%7CSintony:400,700" rel="stylesheet" type="text/css" href="">

		<!-- Vendor CSS -->
		<link rel="stylesheet" href="{{ asset('assets/vendor/bootstrap/css/bootstrap.min.css') }}">
		<link rel="stylesheet" href=" {{asset('assets/vendor/font-awesome/css/font-awesome.min.css')}} ">
		<link rel="stylesheet" href=" {{asset('assets/vendor/animate/animate.min.css')}} ">
		<link rel="stylesheet" href=" {{asset('assets/vendor/simple-line-icons/css/simple-line-icons.min.css')}} ">
		<link rel="stylesheet" href=" {{asset('assets/vendor/owl.carousel/assets/owl.carousel.min.css')}} ">
		<link rel="stylesheet" href=" {{asset('assets/vendor/owl.carousel/assets/owl.theme.default.min.css')}} ">
		<link rel="stylesheet" href=" {{asset('vendor/magnific-popup/magnific-popup.min.css')}} ">

		<!-- Theme CSS -->
		<link rel="stylesheet" href=" {{asset('assets/css/theme.css')}}" >
		<link rel="stylesheet" href=" {{asset('assets/css/theme-elements.css')}}">
		<link rel="stylesheet" href=" {{asset('assets/css/theme-blog.css')}}">
		<link rel="stylesheet" href=" {{asset('assets/css/theme-shop.css')}}">

		<!-- Current Page CSS -->
		<link rel="stylesheet" href=" {{asset('assets/vendor/rs-plugin/css/settings.css')}} ">
		<link rel="stylesheet" href=" {{asset('assets/vendor/rs-plugin/css/layers.css')}} ">
		<link rel="stylesheet" href=" {{asset('assets/vendor/rs-plugin/css/navigation.css')}} ">

		<!-- Skin CSS -->
		<link rel="stylesheet" href=" {{asset('assets/css/skins/skin-business-consulting.css')}} "> 

		<!-- Demo CSS -->
		<link rel="stylesheet" href=" {{asset('assets/css/demos/demo-business-consulting.css')}} ">

		<!-- Theme Custom CSS -->
		<link rel="stylesheet" hhref=" {{asset('assets/css/custom.css')}} ">

		<!-- Head Libs -->
		<script  src= " {{asset('assets/vendor/modernizr/modernizr.min.js')}} "></script>

</head>
<body>

	<div class="body">

			<header id="header" class="header-narrow header-semi-transparent header-transparent-sticky-deactive custom-header-transparent-bottom-border header-flex" data-plugin-options="{'stickyEnabled': true, 'stickyEnableOnBoxed': true, 'stickyEnableOnMobile': true, 'stickyStartAt': 1, 'stickySetTop': '1'}">

				<div class="header-body">

					<div class="header-container container">

						<div class="header-row">

							<div class="header-column">

								<div class="header-logo">

									<a href="demo-business-consulting.html">

										<img alt="" width="82" height="40" src=" {{asset('assets/img/logo/logo.jpg')}} ">

									</a>

								</div>

							</div>

							<div class="header-column">

								<div class="header-row">

									<div class="header-nav header-nav-dark-dropdown">

										<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">

											<i class="fa fa-bars"></i>

										</button>

										<div class="header-nav-main header-nav-main-square custom-header-nav-main-effect-1 collapse">

											<nav>

												<ul class="nav nav-pills" id="mainNav">

													<li class="active">

														<a href="/">

															Home

														</a>

													</li>

													<li>

														<a href="/stakeholder/team">

															Team

														</a>

													</li>

													<li>

														<a href="/stakeholder/kontak">

															Contact Us

														</a>

													</li>
													
													<li>
														<a href="/stakeholder/login">

															Login

														</a>

													</li>

												</ul>

											</nav>

										</div>

									</div>

								</div>

							</div>

						</div>

					</div>

				</div>

			</header>
		
@yield('content')





			<footer id="footer">
				<div class="container">
					<div class="row">
						<div class="col-md-12 center">
							<p>2018 © Corporate <span class="text-color-light">Software Houser</span> - Copyright All Rights Reserved</p>
						</div>
					</div>
				</div>
			</footer>
</div>
		<script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
		<script src="{{ asset('assets/vendor/jquery.appear/jquery.appear.min.js') }}"></script>
		<script src="{{ asset('assets/vendor/jquery.easing/jquery.easing.min.js') }}"></script>
		<script src="{{asset('assets/vendor/jquery-cookie/jquery-cookie.min.js')}}"></script>
		<script src="{{asset('assets/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
		<script src="{{asset('assets/vendor/common/common.min.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery.validation/jquery.validation.min.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.min.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery.gmap/jquery.gmap.min.js')}}"></script>
		<script src="{{asset('assets/vendor/jquery.lazyload/jquery.lazyload.min.js')}}"></script>
		<script src="{{asset('assets/vendor/isotope/jquery.isotope.min.js')}}"></script>
		<script src="{{asset('assets/vendor/owl.carousel/owl.carousel.min.js')}}"></script>
		<script src="{{asset('assets/vendor/magnific-popup/jquery.magnific-popup.min.js')}}"></script>
		<script src="{{asset('assets/vendor/vide/vide.min.js')}}"></script>
		
		<!-- Theme Base, Components and Settings -->
		<script src="{{asset('assets/js/theme.js')}}"></script>
		
		<!-- Current Page Vendor and Views -->
		<script src="{{asset('assets/vendor/rs-plugin/js/jquery.themepunch.tools.min.js')}}"></script>
		<script src="{{asset('assets/vendor/rs-plugin/js/jquery.themepunch.revolution.min.js')}}"></script>

		<!-- Demo -->
		<script src="{{asset('assets/js/demos/demo-business-consulting.js')}}"></script>
		
		<!-- Theme Custom -->
		<script src="{{asset('assets/js/custom.js')}}"></script>
		
		<!-- Theme Initialization Files -->
		<script src="{{asset('assets/js/theme.init.js')}}"></script>


</body>
</html>