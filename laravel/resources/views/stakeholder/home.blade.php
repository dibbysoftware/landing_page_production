@extends('layouts.stakeholder')
@section('content')
			<div role="main" class="main">

 				<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header mb-none">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h1>Dashboard</h1>
								<ul class="breadcrumb breadcrumb-valign-mid">
									<li><a href="#">Home</a></li>
									<li class="active">Dashboard</li>
								</ul>
							</div>
						</div>
					</div>
				</section>



				<div class="container">
					<div class="row pt-xs pb-xl mb-md">
						<div class="col-md-12">

							<ul class="nav nav-pills sort-source mb-xlg" data-sort-id="portfolio" data-option-key="filter" data-plugin-options="{'layoutMode': 'masonry', 'filter': '*'}">
								<li data-option-value="*" class="active"><a class="btn btn-borders custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase" href="#">Show All</a></li>
								<li data-option-value=".pendidikan"><a class="btn btn-borders custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase" href="#">Pendidikan</a></li>
								<li data-option-value=".strategic"><a class="btn btn-borders custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase" href="#">Strategic</a></li>
								<li data-option-value=".tech"><a class="btn btn-borders custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase" href="#">Tech</a></li>
							</ul>

							<div class="row">

								<div class="sort-destination-loader sort-destination-loader-showing">

									<ul class="portfolio-list sort-destination" data-sort-id="portfolio">
										<li class="col-md-4 isotope-item pendidikan">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-1.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Corporate Finance</span>
															<span class="custom-thumb-info-desc font-weight-light">Okler Themes</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>

										<li class="col-md-4 isotope-item pendidikan">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-2.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Corporate Restructuring</span>
															<span class="custom-thumb-info-desc font-weight-light">Envato</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
										<li class="col-md-4 isotope-item strategic">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-3.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Economic Consultanting</span>
															<span class="custom-thumb-info-desc font-weight-light">Porto</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
										<li class="col-md-4 isotope-item tech">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-4.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Tech Consulting</span>
															<span class="custom-thumb-info-desc font-weight-light">GetCustom</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
										<li class="col-md-4 isotope-item pendidikan">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-5.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Strategic</span>
															<span class="custom-thumb-info-desc font-weight-light">Jet Orange</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
										<li class="col-md-4 isotope-item strategic">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-1.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Litigation</span>
															<span class="custom-thumb-info-desc font-weight-light">Paradoxx</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
										<li class="col-md-4 isotope-item pendidikan">
											<a href="demo-business-consulting-cases-detail.html" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-2.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Consulting</span>
															<span class="custom-thumb-info-desc font-weight-light">iMessenger</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
										<li class="col-md-4 isotope-item tech">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-3.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Brand Consulting</span>
															<span class="custom-thumb-info-desc font-weight-light">Theme Forest</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
										<li class="col-md-4 isotope-item pendidikan">
											<a href="#" class="text-decoration-none">
												<span class="thumb-info custom-thumb-info-style-1 mb-xlg pb-xs thumb-info-hide-wrapper-bg">
													<span class="thumb-info-wrapper m-none">
														<img src="img/demos/business-consulting/cases/case-4.jpg" class="img-responsive" alt="">
													</span>
													<span class="thumb-info-caption background-color-secondary p-md pt-xlg pb-xlg">
														<span class="custom-thumb-info-title">
															<span class="custom-thumb-info-name font-weight-semibold text-color-dark text-md">Corporate Consulting</span>
															<span class="custom-thumb-info-desc font-weight-light">Tucson</span>
														</span>
														<span class="custom-arrow"></span>
													</span>
												</span>
											</a>
										</li>
									</ul>
								</div>
							</div>

						</div>
					</div>
				</div>

		<section class="section section-text-light section-background m-none" style="background: url('{{asset('assets/img/background/green1.jpg')}}'); background-size: cover;">

					<div class="container">

						<div class="row">

							<div class="col-md-12">

								<h2 class="font-weight-bold">Contact Us</h2>

								<p class="font-weight-bold">www.dibby.com</p>

								<div class="col-md-6 pl-none">

									<h4 class="mb-xs">Call Us</h4>

									<a href="tel:+1234567890" class="text-decoration-none" target="_blank" title="Call Us">

										<span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">

											Phone

											<span class="font-weight-bold">

												0822-2000-7225

											</span>

										</span>

									</a>

								</div>

								<div class="col-md-6 pl-none custom-sm-margin-top">

									<h4 class="mb-xs">Our Location</h4>

									<p class="font-weight-bold">jl.basudewa no 17 larangan gayam sukoharjo</p>

								</div>

								<div class="col-md-6 pl-none">

									<h4 class="mb-xs">Mail Us</h4>

									<a href="gmail:dibbysoftware@gmail.com" class="text-decoration-none" target="_blank" title="Mail Us">

										<span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">

											Email

											<span class="font-weight-bold">

												dibbysoftware@gmail.com

											</span>

										</span>

									</a>

								</div>

								<div class="col-md-6 pl-none custom-sm-margin-top">

									<h4 class="mb-xs">Social Media</h4>

									<ul class="social-icons custom-social-icons-style-1 custom-opacity-font">

										<li class="social-icons-facebook">

											<a href="#" target="_blank" title="Facebook">

												<i class="fa fa-facebook"></i>

											</a>

										</li>

										<li class="social-icons-twitter">

											<a href="#" target="_blank" title="Twitter">

												<i class="fa fa-twitter"></i>

											</a>

										</li>

										<li class="social-icons-instagram">

											<a href="#" target="_blank" title="Instagram">

												<i class="fa fa-instagram"></i>

											</a>

										</li>

										<li class="social-icons-linkedin">

											<a href="#" target="_blank" title="Linkedin">

												<i class="fa fa-linkedin"></i>

											</a>

										</li>

									</ul>

								</div>

							</div>
						</div>

					</div>

				</section>


			</div>
@endsection