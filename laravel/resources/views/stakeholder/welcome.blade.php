@extends('layouts.landing')
@section('content')

			<div role="main" class="main">

				

				<div class="slider-container rev_slider_wrapper" style="height: 100%;">

					<div id="revolutionSlider" class="slider rev_slider manual">

						<ul>

							<li data-transition="fade">

								<img src="img/background/back_fix2.jpg"  

									alt=""

									data-bgposition="center center" 

									data-bgfit="cover" 

									data-bgrepeat="no-repeat" 

									data-bgparallax="1" 

									class="rev-slidebg">



								<h1 class="tp-caption custom-secondary-font font-weight-bold text-color-light"

									data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"

									data-y="center" data-voffset="['-80','-80','-80','-40']"

									data-start="800"

									data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 32px;">Bangun</h1>



								<div class="tp-caption custom-secondary-font font-weight-bold text-color-light"

									data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"

									data-y="center" data-voffset="['-42','-42','-42','2']"

									data-start="800"

									data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 42px;">Aplikasi dan Website Anda</div>



							</li>

							<li data-transition="fade">

								<img src="img/background/back.jpg"  

									alt=""

									data-bgposition="center center" 

									data-bgfit="cover" 

									data-bgrepeat="no-repeat" 

									data-bgparallax="1" 

									class="rev-slidebg">



								<h1 class="tp-caption custom-secondary-font font-weight-bold text-color-light"

									data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"

									data-y="center" data-voffset="['-80','-80','-80','-40']"

									data-start="800"

									data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 32px;">Dengan Beberapa</h1>



								<div class="tp-caption custom-secondary-font font-weight-bold text-color-light"

									data-x="['left','left','left','left']" data-hoffset="['30','30','30','30']"

									data-y="center" data-voffset="['-42','-42','-42','2']"

									data-start="800"

									data-transform_in="y:[-300%];opacity:0;s:500;" style="font-size: 42px;">Fitur Sesuai Kebutuhan Anda </div>



							</li>

						</ul>

					</div>

				</div>



				<section class="looking-for custom-position-1">

					<div class="container custom-md-border-top">

						<div class="row">

							<div class="col-sm-3 col-md-2">

								<a class="text-decoration-none" href="tel:+00112304567" target="_blank" title="Call Us Now">

									<span class="custom-call-to-action mt-xlg">

										<span class="action-title text-color-primary">Call Us Now</span>

										<span class="action-info text-color-light">+62 8222 0007 225</span>

									</span>

								</a>

							</div>

							<div class="col-sm-3 col-md-2">

								<a class="text-decoration-none" href="mail:mail@example.com" target="_blank" title="Email Us Now">

									<span class="custom-call-to-action mt-xlg">

										<span class="action-title text-color-primary">Email Us Now</span>
										<span class="action-info text-color-light">dibbysoftware@gmail.com</span>

									</span>
								</a>
							</div>
						</div>
					</div>
				</section>



				<section class="section-secondary custom-section-padding">

					<div class="container">

						<div class="row">

							<div class="col-md-12">

								<h2 class="font-weight-bold text-color-dark mb-md">Bidang Kami</h2>

							</div>

						</div>

						<div class="row">

							<div class="col-md-4">

								<a href="#" class="text-decoration-none appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="0">

									<div class="feature-box custom-feature-box feature-box-style-2">

										<div class="feature-box-icon">

											<img  width="141 px" alt="" src=" {{asset('assets/img/background/secure.png')}} ">

										</div>

										<div class="feature-box-info ml-md">

											<h4 class="font-weight-normal text-lg">Dekstop Software</h4>

											<p align="justify">Melayani pembuatan software komputer untuk pencatatan transaksi, mesin kasir, inventory, absensi, pengiriman barang, DLL.</p>

										</div>

									</div>

								</a>

							</div>

							<div class="col-md-4">

								<a href="#" class="text-decoration-none appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="100">

									<div class="feature-box custom-feature-box feature-box-style-2">

										<div class="feature-box-icon">

											<img width="141 px" alt="" src=" {{asset('assets/img/background/box.jpg')}} ">

										</div>

										<div class="feature-box-info ml-md">

											<h4 class="font-weight-normal text-lg">Mobile Android</h4>

											<p align="justify">Melayani pembuatan mobile app android untuk online booking, monitoring penjualan, pengaduan online, peminjaman buku, DLL.</p>

										</div>

									</div>

								</a>

							</div>

							<div class="col-md-4">

								<a href="#" class="text-decoration-none appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="200">

									<div class="feature-box custom-feature-box feature-box-style-2">

										<div class="feature-box-icon">

											<img src=" {{asset('assets/img/background/app.jpg')}} " alt="">
											

										</div>

										<div class="feature-box-info ml-md">

											<h4 class="font-weight-normal text-lg">Website</h4>

											<p align="justify">Melayani pembuatan website, blog, online shop, profile perusahaan, online booking, peminjaman buku, DLL.</p>

										</div>

									</div>

								</a>

							</div>

						</div>

					</div>

				</section>



				<section class="custom-section-padding">

					<div class="container">

						<div class="row">

							<div class="col-md-12">

								<h2 class="font-weight-bold text-color-dark">Strategi Kami</h2>

								<div class="owl-carousel owl-theme nav-bottom rounded-nav numbered-dots pl-xs pr-xs" data-plugin-options="{'items': 1, 'loop': false, 'dots': true, 'nav': false}">

									<div>

										<div class="custom-step-item">

											<span class="step text-uppercase">

												Langkah

												<span class="step-number text-color-primary">

													01

												</span>

											</span>

											<div class="step-content">

												<h4 class="mb-xlg">The first meeting<br> <strong>Pemahaman tentang permasalahan dan perancangan</strong></h4>

												<p>Pada langkah pertama ini kita melakukan pertemuan dengan stakeholder untuk membahas tentang kebutuhan aplikasi,untuk merancang bagaimana aplikasi ini dapat berjalan sesuai dengan kebutuhan stakeholder. </p>

											</div>

										</div>

									</div>

									<div>

										<div class="custom-step-item">

											<span class="step text-uppercase">

												Step

												<span class="step-number text-color-primary">

													02

												</span>

											</span>

											<div class="step-content">

												<h4 class="mb-xlg">The second <br> <strong>Prototyping dan development</strong></h4>

												<p>Pada tahap kedua ini, kita melakukan penggambaran sistem dengan melakukan prototyping, apabila prototype telah disetujui, maka kita akan langsung membangun aplikasi tersebut.</p>

											</div>

										</div>

									</div>

									<div>

										<div class="custom-step-item">

											<span class="step text-uppercase">

												Step

												<span class="step-number text-color-primary">

													03

												</span>

											</span>

											<div class="step-content">

												<h4 class="mb-xlg">The final<br> <strong>Testing</strong></h4>

												<p>Pada tahap terakhir ini, kita melakukan tesing dengan stakholder, apabila aplikasi telah sesuai dengan kebutuhan stakeholder, maka pembangunan aplikasi telah selesai.</p>

											</div>

										</div>

									</div>

								</div>

							</div>


						</div>

					</div>

				</section>







				<section class="section section-text-light section-background m-none" style="background: url('{{asset('assets/img/background/green1.jpg')}}'); background-size: cover;">

					<div class="container">

						<div class="row">

							<div class="col-md-12">

								<h2 class="font-weight-bold">Contact Us</h2>

								<p class="font-weight-bold">www.dibbysoftware.com</p>

								<div class="col-md-6 pl-none">

									<h4 class="mb-xs">Call Us</h4>

									<a href="tel:+1234567890" class="text-decoration-none" target="_blank" title="Call Us">

										<span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">

											Phone

											<span class="font-weight-bold">

												0822-2000-7225

											</span>

										</span>

									</a>

								</div>

								<div class="col-md-6 pl-none custom-sm-margin-top">

									<h4 class="mb-xs">Our Location</h4>

									<p class="font-weight-bold">jl.basudewa no 17 larangan gayam sukoharjo</p>

								</div>

								<div class="col-md-6 pl-none">

									<h4 class="mb-xs">Mail Us</h4>

									<a href="gmail:dibbysoftware@gmail.com" class="text-decoration-none" target="_blank" title="Mail Us">

										<span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">

											Email

											<span class="font-weight-bold">

												dibbysoftware@gmail.com

											</span>

										</span>

									</a>

								</div>

								<div class="col-md-6 pl-none custom-sm-margin-top">

									<h4 class="mb-xs">Social Media</h4>

									<ul class="social-icons custom-social-icons-style-1 custom-opacity-font">

										<li class="social-icons-facebook">

											<a href="#" target="_blank" title="Facebook">

												<i class="fa fa-facebook"></i>

											</a>

										</li>

										<li class="social-icons-twitter">

											<a href="#" target="_blank" title="Twitter">

												<i class="fa fa-twitter"></i>

											</a>

										</li>

										<li class="social-icons-instagram">

											<a href="#" target="_blank" title="Instagram">

												<i class="fa fa-instagram"></i>

											</a>

										</li>

										<li class="social-icons-linkedin">

											<a href="#" target="_blank" title="Linkedin">

												<i class="fa fa-linkedin"></i>

											</a>

										</li>

									</ul>

								</div>

							</div>
						</div>

					</div>

				</section>

			</div> 
@endsection