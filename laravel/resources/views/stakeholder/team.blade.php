@extends('layouts.landing')
@section('content')
			<div role="main" class="main">

				<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header mb-none">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h1>Team <span>Meet our professionals</span></h1>
								<ul class="breadcrumb breadcrumb-valign-mid">
									<li><a href="#">Home</a></li>
									<li class="active">Team</li>
								</ul>
							</div>
						</div>
					</div>
				</section>

			<section class="section-secondary custom-section-padding">
					<div class="container">
						<div class="row">
							<div class="col-md-12">

								<ul class="nav nav-pills sort-source mb-xlg" data-sort-id="team" data-option-key="filter" data-plugin-options="{'layoutMode': 'masonry', 'filter': '*'}">
									<li data-option-value="*" class="active"><a class="btn btn-borders custom-border-width btn-primary custom-border-radius custom-border-radius-small text-uppercase" href="#">Show All</a></li>
								</ul>

								<div class="row">
									<ul class="team-list sort-destination" data-sort-id="team">
										<li class="col-md-4 isotope-item leadership">
											<div class="team-item">
													<span class="image-wrapper">
														<img src="" alt="" class="img-responsive" />
													</span>
												<div class="team-infos">
													<div class="share">
														<i class="fa fa-share-alt"></i>
														<div class="share-icons background-color-light">
															<a href="#" class="text-decoration-none" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
															<a href="#" class="text-decoration-none" title="Share on Instagram"><i class="fa fa-instagram"></i></a>
															<a href="#" class="text-decoration-none" title="Share on Linkedin"><i class="fa fa-linkedin"></i></a>
														</div>
													</div>
													<a href="#" class="text-decoration-none">
														<span class="team-member-name text-color-dark font-weight-semibold text-md">Robby Dwi Hartanto</span>
														<span class="team-member-desc font-weight-light">CEO</span>
													</a>
												</div>
											</div>
										</li>


										<li class="col-md-4 isotope-item other">
											<div class="team-item">
													<span class="image-wrapper">
														<img src="" alt="" class="img-responsive" />
													</span>
												<div class="team-infos">
													<div class="share">
														<i class="fa fa-share-alt"></i>
														<div class="share-icons background-color-light">
															<a href="https://www.facebook.com/adylla.cfatyah" class="text-decoration-none" title="Share on Facebook"><i class="fa fa-facebook"></i></a>
															<a href="https://www.instagram.com/adila_fatyah/" class="text-decoration-none" title="Share on Instagram"><i class="fa fa-instagram"></i></a>
															<a href="#" class="text-decoration-none" title="Share on Linkedin"><i class="fa fa-linkedin"></i></a>
														</div>
													</div>
													<a href="#" class="text-decoration-none">
														<span class="team-member-name text-color-dark font-weight-semibold text-md">Adila Chusnul Fatiyah</span>
														<span class="team-member-desc font-weight-light">Finance Expert</span>
													</a>
												</div>
											</div>
										</li>
									</ul>
								</div>

							</div>
						</div>
					</div>
				</section>
	</div>
@endsection