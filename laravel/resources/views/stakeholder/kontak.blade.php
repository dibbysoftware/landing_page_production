@extends('layouts.landing')
@section('content')
			<div role="main" class="main">

 				<section class="page-header page-header-color page-header-quaternary page-header-more-padding custom-page-header mb-none">
					<div class="container">
						<div class="row">
							<div class="col-md-12">
								<h1>Contact Us</h1>
								<ul class="breadcrumb breadcrumb-valign-mid">
									<li><a href="#">Home</a></li>
									<li class="active">Contact Us</li>
								</ul>
							</div>
						</div>
					</div>
				</section>



		<section class="section section-text-light section-background m-none" style="background: url('{{asset('assets/img/background/green1.jpg')}}'); background-size: cover;">

					<div class="container">

						<div class="row">

							<div class="col-md-12">

								<h2 class="font-weight-bold">Contact Us</h2>

								<p class="font-weight-bold">www.dibby.com</p>

								<div class="col-md-6 pl-none">

									<h4 class="mb-xs">Call Us</h4>

									<a href="tel:+1234567890" class="text-decoration-none" target="_blank" title="Call Us">

										<span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">

											Phone

											<span class="font-weight-bold">

												0822-2000-7225

											</span>

										</span>

									</a>

								</div>

								<div class="col-md-6 pl-none custom-sm-margin-top">

									<h4 class="mb-xs">Our Location</h4>

									<p class="font-weight-bold">jl.basudewa no 17 larangan gayam sukoharjo</p>

								</div>

								<div class="col-md-6 pl-none">

									<h4 class="mb-xs">Mail Us</h4>

									<a href="gmail:dibbysoftware@gmail.com" class="text-decoration-none" target="_blank" title="Mail Us">

										<span class="custom-call-to-action-2 text-color-light text-sm custom-opacity-font">

											Email

											<span class="font-weight-bold">

												dibbysoftware@gmail.com

											</span>

										</span>

									</a>

								</div>

								<div class="col-md-6 pl-none custom-sm-margin-top">

									<h4 class="mb-xs">Social Media</h4>

									<ul class="social-icons custom-social-icons-style-1 custom-opacity-font">

										<li class="social-icons-facebook">

											<a href="#" target="_blank" title="Facebook">

												<i class="fa fa-facebook"></i>

											</a>

										</li>

										<li class="social-icons-twitter">

											<a href="#" target="_blank" title="Twitter">

												<i class="fa fa-twitter"></i>

											</a>

										</li>

										<li class="social-icons-instagram">

											<a href="#" target="_blank" title="Instagram">

												<i class="fa fa-instagram"></i>

											</a>

										</li>

										<li class="social-icons-linkedin">

											<a href="#" target="_blank" title="Linkedin">

												<i class="fa fa-linkedin"></i>

											</a>

										</li>

									</ul>

								</div>

							</div>
						</div>

					</div>

				</section>
</div>
@endsection