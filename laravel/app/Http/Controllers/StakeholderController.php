<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StakeholderController extends Controller
{
    public function home(){
    	return view('stakeholder/welcome');
    }
    public function login(){
    	return view('stakeholder/login');
    }
    public function kontak(){
    	return view('stakeholder/kontak');
    }
    public function team(){
    	return view('stakeholder/team');
    }

}
