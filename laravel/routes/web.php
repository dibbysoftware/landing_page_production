<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
//auth dimatika
//Auth::routes();

Route::get('/', 'StakeholderController@home');
Route::get('/stakeholder/login', 'StakeholderController@login');
Route::get('/stakeholder/kontak', 'StakeholderController@kontak');
Route::get('/stakeholder/team', 'StakeholderController@team');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
